# Tu tienda webapp #

### An e-commerce for a super market ###

* Version: 1.0.0

### How do I get set up? ###

* Clone this repository.
* Run in your terminal: `npm install`

### Scripts ###

* Build app: `npm run build`
* Start dev server: `npm start`
* Run unit tests and coverage: `npm run test`

### Contact ###

* [Leonardo Ruiz Diaz](mailto:leonardord8@gmail.com)