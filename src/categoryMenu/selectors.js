import constants from './constants';

const getCategories = state => state[constants.NAME].categories;

export default getCategories;
