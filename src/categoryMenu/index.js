import actions from './actions';
import actionTypes from './actionTypes';
import CategoryMenu from './CategoryMenu.jsx';
import constants from './constants';
import reducers from './reducers';
import selectors from './selectors';

export default {
  actions, actionTypes, constants, CategoryMenu, reducers, selectors,
};
