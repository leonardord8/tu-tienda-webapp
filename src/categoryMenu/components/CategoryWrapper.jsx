import React from 'react';
import PropTypes from 'prop-types';
import SubCategoryWrapper from './SubCategoryWrapper.jsx';

const CategoryWrapper = (props) => (
  <li
    className='flyout-alt category'
  >
    <a>
      {props.name}
    </a>
    <ul
      className='flyout-content nav stacked'
    >
      {
        props.sublevels.map((subCategory) => (
          <SubCategoryWrapper
            key={subCategory.id}
            {...subCategory}
          />
        ))
      }
    </ul>
  </li>
);

CategoryWrapper.propTypes = {
  name: PropTypes.string.isRequired,
  sublevels: PropTypes.array.isRequired,
};

export default CategoryWrapper;
