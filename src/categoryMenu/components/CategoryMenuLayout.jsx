import React from 'react';
import PropTypes from 'prop-types';

const CategoryMenuLayout = props => (
  <ul
    className='nav site-nav'
  >
    <li
      className='flyout'
    >
      <a>
        Categorias
      </a>
      <ul
        className='flyout-content nav stacked'
      >
        {props.children}
      </ul>
    </li>
  </ul>
);

CategoryMenuLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default CategoryMenuLayout;
