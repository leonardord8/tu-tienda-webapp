import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const SubCategoryWrapper = (props) => {
  const isFlyout = props.sublevels ? 'flyout-alt' : '';
  return (
    <li
      className={isFlyout}
    >
      <Link
        to={`/category/${props.id}`}
      >
        {props.name}
      </Link>
      {
        props.sublevels &&
        <ul
          className='flyout-content nav stacked'
        >
          {
            props.sublevels.map((subCategory) => (
              <SubCategoryWrapper
                key={subCategory.id}
                {...subCategory}
              />
            ))
          }
        </ul>
      }
    </li>
  );
};

SubCategoryWrapper.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  sublevels: PropTypes.array,
};

SubCategoryWrapper.defaultProps = {
  sublevels: null,
}

export default SubCategoryWrapper;
