import actionTypes from './actionTypes';

const setCategories = payload => ({
  type: actionTypes.SET_CATEGORIES,
  payload,
});

export default {
  setCategories,
};
