import { combineReducers } from 'redux';
import actionTypes from './actionTypes';

const initialCategoryMenuState = {
  categories: [],
};

const setCategoriesReducer = (state = initialCategoryMenuState.categories, action = {}) => {
  switch (action.type) {
    case actionTypes.SET_CATEGORIES:
      return action.payload;
    default:
      return state;
  }
};

export default combineReducers({
  categories: setCategoriesReducer,
});
