import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import categoriesAction from './actions';
import CategoryMenuLayout from './components/CategoryMenuLayout.jsx';
import CategoryWrapper from './components/CategoryWrapper.jsx';
import categoriesClient from '../clients/categoriesClient';
import getCategories from './selectors';
import './categoryMenu.css'

class CategoryMenu extends Component {
  async componentWillMount() {
    const categories = await categoriesClient.getCategories();
    this.props.categoryMenuActions.setCategories(categories);
  }

  getMenus = () => (
    this.props.categories.map((category) => (
      <CategoryWrapper
        key={category.id}
        {...category}
      />
    ))
  )

  render() {
    return (
      <CategoryMenuLayout>
        {this.getMenus()}
      </CategoryMenuLayout>
    );
  }
};

CategoryMenu.propTypes = {
  categoryMenuActions: PropTypes.object.isRequired,
  categories: PropTypes.array.isRequired,
};

const mapStateToProps = () => (
  createStructuredSelector({
    categories: getCategories,
  })
);

const mapDispatchToProps = (dispatch) => (
  {
    categoryMenuActions: bindActionCreators(categoriesAction, dispatch),
  }
);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CategoryMenu);
