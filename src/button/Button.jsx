import React from 'react';
import PropTypes from 'prop-types';

import './button.css';

const Button = props => (
  <button
    className={`btn ${props.className}`}
    onClick={props.onClick}
    type={props.type}
    disabled={props.disabled}
  >
    <label>{props.label}</label>
  </button>
);

Button.propTypes = {
  className: PropTypes.string,
  disabled: PropTypes.bool,
  label: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  type: PropTypes.string,
};

Button.defaultProps = {
  className: '',
  disabled: false,
  label: 'Enviar',
  type: 'button',
};

export default Button;

