import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import header from './header';
import home from './home';
import productList from './productList';
import shoppingCart from './shoppingCart';

import './App.css';

const Header = header.Header;
const Home = home.Home;
const ProductList = productList.ProductList;
const ShoppingCart = shoppingCart.ShoppingCart;

const App = () => (
  <Router >
    <div
      className='page-wrapper'
    >
      <Header />
      <Route
        exact
        path='/'
        component={Home}
      />
      <Route
        path={`/category/:categoryId`}
        component={ProductList}
      />
      <Route
        exact
        path='/my-cart'
        component={ShoppingCart}
      />
    </div>
  </Router>
);

export default App;
