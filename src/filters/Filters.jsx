import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import Filter from './components/Filter.jsx';
import filterHelppers from './filterHelppers/filterHelppers';
import filtersActions from './actions';
import FiltersLayout from './components/FiltersLayout.jsx';
import selectors from './selectors';
import sortter from '../sortter';

const Sortter = sortter.Sortter;

class Filters extends Component {
  onFilterClick = (filter, target) => {
    let newSelectedFilters = []
    if (target.target.checked) {
      newSelectedFilters = filterHelppers.addFilter(filter, this.props.selectedFilters);
    } else {
      newSelectedFilters = filterHelppers.deleteFilter(filter, this.props.selectedFilters)
    }
    this.props.filtersActions.setSelectedFilters(newSelectedFilters);
  }

  renderFilters = () => (
    this.props.existingFilters.map((filter) => (
      <Filter
        key={filter.id}
        {...filter}
        onClick={this.onFilterClick}
      />
    ))
  )

  render() {
    return (
      <FiltersLayout>
        {this.renderFilters()}
        <Sortter />
      </FiltersLayout>
    )
  }
};

Filters.propTypes = {
  selectedFilters: PropTypes.array.isRequired,
  existingFilters: PropTypes.array.isRequired,
  filtersActions: PropTypes.object.isRequired,
};

const mapStateToProps = () =>  (
  createStructuredSelector({
    selectedFilters: selectors.getSelectedFilters,
    existingFilters: selectors.getExistingFilters,
  })
);

const mapDispatchToProps = (dispatch) => ({
  filtersActions: bindActionCreators(filtersActions, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Filters);
