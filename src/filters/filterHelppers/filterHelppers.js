import sortterHelpper from '../../sortter/sortterHelpper/sortterHelpper';

const searchInArray = (key, elementToFind, arrayToSearch) => {
  let index = -1;
  for (let i = 0; i < arrayToSearch.length; i += 1) {
    if (elementToFind[key] === (arrayToSearch[i])[key]) {
      index = i;
      break;
    }
  }
  return index;
};

const addFilter = (filterToAdd, arrayToAdd) => {
  const index = searchInArray('id', filterToAdd, arrayToAdd);
  if (filterToAdd.isAGroup && index !== -1) {
    arrayToAdd[index].value.push(filterToAdd.value[0]);
  } else {
    arrayToAdd.push(filterToAdd);
  }

  return arrayToAdd;
};

const deleteFilter = (filterToDelete, arrayToDelete) => {
  const index = searchInArray('id', filterToDelete, arrayToDelete);
  if (index !== -1) {
    if (filterToDelete.isAGroup) {
      const arrayValue = arrayToDelete[index].value;
      arrayValue.splice(arrayValue.indexOf(filterToDelete.value[0]), 1);
    } else {
      arrayToDelete.splice(index, 1);
    }
  }

  return arrayToDelete;
};

const getFilteredAndOrderedList = (categoryId, allProducts, selectedFilters, sortterCritteria) => {
  let filteredList = allProducts.filter(product => product.sublevel_id === categoryId);
  selectedFilters.map((filter) => {
    if (filter.isAGroup) {
      if (filter.value.length > 0) {
        filteredList = filteredList.filter(product => (
          filter.value.includes(product[filter.fieldToCompare])
        ));
      }
    } else {
      filteredList = filteredList.filter(product => product[filter.fieldToCompare]);
    }

    return null;
  });

  filteredList = sortterHelpper.sortArray(filteredList, sortterCritteria);
  return filteredList;
};

export default {
  addFilter,
  deleteFilter,
  getFilteredAndOrderedList,
};
