import { combineReducers } from 'redux';
import actionTypes from './actionTypes';
import filterTypes from './filterTypes';

const initialFiltersState = {
  selectedFilters: [],
  existingFilters: [...filterTypes],
};

const setSelectedFiltersReducer = (state = initialFiltersState.selectedFilters, action = {}) => {
  switch (action.type) {
    case actionTypes.SET_SELECTED_FILTERS:
      return [...action.payload];
    default:
      return state;
  }
};

const setExistingFiltersReducer = (state = initialFiltersState.existingFilters, action = {}) => {
  switch (action.type) {
    case actionTypes.SET_EXISTING_FILTERS:
      return action.payload;
    default:
      return state;
  }
};

export default combineReducers({
  selectedFilters: setSelectedFiltersReducer,
  existingFilters: setExistingFiltersReducer,
});
