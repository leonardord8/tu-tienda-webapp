import constants from './constants';

const getExistingFilters = state => state[constants.NAME].existingFilters;

const getSelectedFilters = state => state[constants.NAME].selectedFilters;

export default {
  getExistingFilters,
  getSelectedFilters,
};
