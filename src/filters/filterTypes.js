export default [
  {
    id: 1,
    name: 'Disponibilidad',
    fieldToCompare: 'availability',
    isAGroup: false,
    groupValues: null,
  },
  // Ejemplo para agregar otro filtro:
  // {
  //   id: 2,
  // nombre del filtro.
  //   name: 'Nombre',
  // campo a comparar del producto.
  //   fieldToCompare: 'name',
  // true si es un grupo de valores posible, si es un solo valor,
  // settear a false
  //   isAGroup: true,
  // grupo de valores posibles,
  // si es un solo valor settear en null
  //   groupValues: ['non', 'dolor', 'aute'],
  // },
];
