import React from 'react';
import PropTypes from 'prop-types';

const FiltersLayout = (props) => (
  <aside
    className='filters-layout'
  >
    {props.children}
  </aside>
);

FiltersLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default FiltersLayout;
