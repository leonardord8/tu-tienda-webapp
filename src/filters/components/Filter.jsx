import React from 'react';
import PropTypes from 'prop-types';

const Filter = (props) => {
  const inputRender = () => {
    let inputToReturn = null;
    if (props.isAGroup) {
      inputToReturn = props.groupValues.map((value) => (
        <div
          key={value}
        >
          <span>{value}</span>
          <input
            type='checkbox'
            onClick={(e) => props.onClick({...props, value: [value]}, e)}
          />
        </div>
      ))
    } else {
      inputToReturn = (
        <input
          type='checkbox'
          onClick={(e) => props.onClick(props, e)}
        />
      )
    }

    return inputToReturn;
  }
  return (
    <section>
      <span>
        {props.name}
      </span>
      {inputRender()}
    </section>
  )
};

Filter.propTypes = {
  name: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default Filter;
