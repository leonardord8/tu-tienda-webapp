import actionTypes from './actionTypes';

const setSelectedFilters = payload => ({
  type: actionTypes.SET_SELECTED_FILTERS,
  payload,
});

export default {
  setSelectedFilters,
};
