import { createStore, combineReducers } from 'redux';
import categoryMenu from './categoryMenu';
import filters from './filters';
import header from './header';
import home from './home';
import shoppingCart from './shoppingCart';
import sortter from './sortter';

const combinedReducers = combineReducers({
  [categoryMenu.constants.NAME]: categoryMenu.reducers,
  [filters.constants.NAME]: filters.reducers,
  [header.constants.NAME]: header.reducers,
  [home.constants.NAME]: home.reducers,
  [sortter.constants.NAME]: sortter.reducers,
  [shoppingCart.constants.NAME]: shoppingCart.reducers,
});

const store = createStore(combinedReducers);

export default store;
