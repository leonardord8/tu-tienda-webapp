import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import sortterActions from './actions';
import SortterLayout from './components/SortterLayout.jsx';
import './sortter.css';

class Sortter extends Component {
  onSortterChange = (event) => {
    this.props.sortterActions.setSortterCritteria(event.target.value);
  };

  render() {
    return (
      <SortterLayout
        onChange={this.onSortterChange}
      >
        <option value='none' default>No ordenar.</option>
        <option value='upward-price'>Menor Precio.</option>
        <option value='backward-price'>Mayor Precio.</option>
        <option value='upward-quantity'>Menor Cantidad.</option>
        <option value='backward-quantity'>Mayor Cantidad.</option>
      </SortterLayout>
    );
  };
};

Sortter.propTypes = {
  sortterActions: PropTypes.object.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
  sortterActions: bindActionCreators(sortterActions, dispatch),
});

export default connect(
  null,
  mapDispatchToProps,
)(Sortter);
