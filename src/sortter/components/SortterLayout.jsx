import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

const SortterLayout = (props) => (
  <Fragment>
    <span>
      Ordenar por:
    </span>
    <select
      className='select-sortter'
      onChange={props.onChange}
    >
      {props.children}
    </select>
  </Fragment>
);

SortterLayout.propTypes = {
  children: PropTypes.node.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default SortterLayout;
