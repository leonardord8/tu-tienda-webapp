import constants from './constants';

const getSortterCritteria = state => state[constants.NAME].sortterCritteria;

export default getSortterCritteria;
