import { combineReducers } from 'redux';
import actionTypes from './actionTypes';

const initialSortterState = {
  sortterCritteria: null,
};

const setSortterCritteriaReducer = (state = initialSortterState.sortterCritteria, action = {}) => {
  switch (action.type) {
    case actionTypes.SET_SORTTER_CRITTERIA:
      return action.payload;
    default:
      return state;
  }
};

export default combineReducers({
  sortterCritteria: setSortterCritteriaReducer,
});
