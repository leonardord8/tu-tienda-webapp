const parse = str => parseFloat(str.slice(1));

const sortUpwardPrice = (a, b) => {
  if (parse(a.price) > parse(b.price)) {
    return 1;
  }
  if (parse(a.price) < parse(b.price)) {
    return -1;
  }
  return 0;
};

const sortBackwardPrice = (a, b) => {
  if (parse(a.price) < parse(b.price)) {
    return 1;
  }
  if (parse(a.price) > parse(b.price)) {
    return -1;
  }
  return 0;
};

const sortUpwardQuantity = (a, b) => {
  if (a.quantity > b.quantity) {
    return 1;
  }
  if (a.quantity < b.quantity) {
    return -1;
  }
  return 0;
};

const sortBackwardQuantity = (a, b) => {
  if (a.quantity < b.quantity) {
    return 1;
  }
  if (a.quantity > b.quantity) {
    return -1;
  }
  return 0;
};

const notSort = () => 0;

const sortArray = (arrayToSort, sortterCritteria) => {
  let functionToSort = notSort;
  switch (sortterCritteria) {
    case 'upward-price':
      functionToSort = sortUpwardPrice;
      break;
    case 'backward-price':
      functionToSort = sortBackwardPrice;
      break;
    case 'upward-quantity':
      functionToSort = sortUpwardQuantity;
      break;
    case 'backward-quantity':
      functionToSort = sortBackwardQuantity;
      break;
    default:
      break;
  }

  return arrayToSort.sort(functionToSort);
};

export default {
  sortArray,
};
