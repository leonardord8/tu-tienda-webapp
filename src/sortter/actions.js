import actionTypes from './actionTypes';

const setSortterCritteria = payload => ({
  type: actionTypes.SET_SORTTER_CRITTERIA,
  payload,
});

export default {
  setSortterCritteria,
};
