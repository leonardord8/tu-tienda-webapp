import React from 'react';

const EmptyProductList = () => (
  <section
    className='empty-product-list'
  >
    <h2>
      No se encuentran productos para esta categoria.
    </h2>
  </section>
);

export default EmptyProductList;
