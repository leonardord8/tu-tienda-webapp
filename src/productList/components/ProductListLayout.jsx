import React from 'react';
import PropTypes from 'prop-types';

const ProductListLayout = (props) => (
  <div
    className='body-content product-list'
  >
    {props.children}
  </div>
);

ProductListLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ProductListLayout;
