import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import PropTypes from 'prop-types';
import EmptyProductList from './components/EmptyProductList.jsx';
import filterHelppers from '../filters/filterHelppers/filterHelppers.js';
import filters from '../filters';
import home from '../home';
import itemCard from '../itemCard';
import ProductListLayout from './components/ProductListLayout.jsx';
import sortter from '../sortter';
import './productList.css';

const Filters = filters.Filters;
const ItemCard = itemCard.ItemCard;

class ProductList extends Component {
  renderContent = () => {
    if (this.props.productList.length === 0) {
      return <EmptyProductList />;
    };

    return (
      <div>
        {
          this.props.productList.map((product) => (
            <ItemCard
              className='product-by-category'
              key={product.id}
              {...product}
            />
          ))
        }
      </div>
    );
  }

  render() {
    return (
      <ProductListLayout>
        <Filters />
        {this.renderContent()}
      </ProductListLayout>
    );
  }
};

ProductList.propTypes = {
  productList: PropTypes.array,
};

ProductList.defaultProps = {
  productList: null,
};

const mapStateToProps = () => {
  return (
    createSelector(
      [
        (state, props) => parseInt(props.match.params.categoryId),
        home.selectors,
        filters.selectors.getSelectedFilters,
        sortter.selectors,
      ],
      (categoryId, allProducts, selectedFilters, sortterCritteria) => ({
        productList: filterHelppers.getFilteredAndOrderedList(categoryId, allProducts, selectedFilters, sortterCritteria),
      })
    )
  )
};

export default connect(
  mapStateToProps,
)(ProductList)
