import React from 'react';
import PropTypes from 'prop-types';

const ProductName = props => (
  <p
    className='product-name'
  >
    {props.productName}
  </p>
);

ProductName.propTypes = {
  productName: PropTypes.string.isRequired,
};

export default ProductName;
