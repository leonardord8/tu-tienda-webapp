import React from 'react';
import PropTypes from 'prop-types';

const ItemCardLayout = props => (
  <section
    className={`item-card-layout ${props.className}`}
  >
    {props.children}
  </section>
);

ItemCardLayout.proptypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};

ItemCardLayout.defaultProps = {
  className: '',
};

export default ItemCardLayout;
