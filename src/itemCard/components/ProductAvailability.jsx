import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

const ProductAvailability = props => {
  const isAvailable = props.availability ? 'Disponible' : 'Sin stock';
  return (
    <Fragment>
      <p>{isAvailable}</p>
      {
        props.availability &&
        <p
          className="product-quantity"
        >
          Cantidad: {props.quantity}
        </p>
      }
    </Fragment>
  );
};

ProductAvailability.propTypes = {
  availability: PropTypes.bool.isRequired,
};

export default ProductAvailability;
