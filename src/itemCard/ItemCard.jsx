import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Button from '../button/Button.jsx';
import { checkItemInCart, setLocalStorage, getLocalStorage } from '../helppers/localStorageHelpper';
import header from '../header';
import ItemCardLayout from './components/ItemCardLayout.jsx';
import localStorageKey from '../helppers/constants';
import ProductName from './components/ProductName.jsx';
import ProductAvailability from './components/ProductAvailability.jsx';

import './itemCard.css';

const headerActions = header.actions;

class ItemCard extends Component {
  onAddItemToCart = () => {
    const id = this.props.id;

    let currentLocalStorage = getLocalStorage(localStorageKey);
    if (!checkItemInCart(id, currentLocalStorage)) {
      currentLocalStorage.push(id);
      setLocalStorage(localStorageKey, currentLocalStorage);
      this.props.headerActions.setItemQuantity(currentLocalStorage.length);
      return;
    }

    window.alert('El item ya se encuentra en su carrito');
  }

  render() {
    return (
      <ItemCardLayout
        className={this.props.className}
      >
        <ProductName
          productName={this.props.name}
        />
        <ProductAvailability
          availability={this.props.availability}
          quantity={this.props.quantity}
        />
        <p>Precio: {this.props.price}</p>
        <Button
          className='btn-primary'
          label='Agregar a carrito'
          onClick={this.onAddItemToCart}
          disabled={!this.props.availability}
        />
      </ItemCardLayout>
    )
  }
};

ItemCard.propTypes = {
  availability: PropTypes.bool.isRequired,
  id: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
};

const mapDispatchToProps = (dispatch) => ({
  headerActions: bindActionCreators(headerActions, dispatch),
});

export default connect(
  null,
  mapDispatchToProps,
)(ItemCard);
