import axios from 'axios';
import baseUrl from './baseUrl';

const getProducts = async () => {
  let resp = [];
  await axios.get(`${baseUrl}/products`)
    .then((response) => { resp = response.data.products; })
    .catch((error) => {
      console.error(error);
    });
  return resp;
};

export default {
  getProducts,
};
