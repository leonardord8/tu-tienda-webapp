import axios from 'axios';
import baseUrl from './baseUrl';

const getCategories = async () => {
  let resp = [];
  await axios.get(`${baseUrl}/categories`)
    .then((response) => { resp = response.data.categories; })
    .catch((error) => {
      console.error(error);
    });
  return resp;
};

export default {
  getCategories,
};
