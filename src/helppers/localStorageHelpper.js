export const getLocalStorage = (value) => {
  const response = localStorage.getItem(value);
  if (response) {
    return JSON.parse(response);
  }

  return [];
};

export const setLocalStorage = (key, value) => localStorage.setItem(key, JSON.stringify(value));

export const checkItemInCart = (item, cart) => cart.includes(item);
