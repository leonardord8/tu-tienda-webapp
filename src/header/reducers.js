import { combineReducers } from 'redux';
import actionTypes from './actionTypes';

const initialHeaderState = {
  itemQuantity: 0,
};

const setItemQuantityReducer = (state = initialHeaderState.itemQuantity, action = {}) => {
  switch (action.type) {
    case actionTypes.SET_ITEM_QUANTITY:
      return action.payload;
    default:
      return state;
  }
};

export default combineReducers({
  itemQuantity: setItemQuantityReducer,
});
