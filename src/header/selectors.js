import constants from './constants';

const getItemQuantity = state => state[constants.NAME].itemQuantity;

export default getItemQuantity;
