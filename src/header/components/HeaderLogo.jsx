import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../../../assets/logo.png';

const HeaderLogo = () => (
  <div
    className="page-logo"
  >
    <Link
      to="/"
    >
      <img
        src={logo}
        alt="logo"
        className="default-logo"
      />
    </Link>
  </div>
);

export default HeaderLogo;
