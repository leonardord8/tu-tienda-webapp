import React from 'react';
import PropTypes from 'prop-types';

const HeaderLayout = props => (
  <div
    className="page-header navbar-fixed-top"
  >
    {props.children}
  </div>
);

HeaderLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default HeaderLayout;
