import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { FaShoppingCart } from 'react-icons/fa';

const ShoppingCart = props => (
  <div
    className='shopping-cart-header'
  >
    <Link
      to='/my-cart'
      className='my-cart-link'
    >
      <FaShoppingCart
        color='#a4aebb'
        size='1.2em'
        className='my-cart-icon'
      />
      <span
        className='badge badge-default'
      >
        { props.itemQuantity }
      </span>
    </Link>
  </div>
);

ShoppingCart.propTypes = {
  itemQuantity: PropTypes.number,
};

ShoppingCart.defaultProps = {
  itemQuantity: 0,
};

export default ShoppingCart;
