import actionTypes from './actionTypes';

const setItemQuantity = payload => ({
  type: actionTypes.SET_ITEM_QUANTITY,
  payload,
});

export default {
  setItemQuantity,
};
