import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import Proptypes from 'prop-types';
import categoryMenu from '../categoryMenu';
import getItemQuantity from './selectors';
import { getLocalStorage } from '../helppers/localStorageHelpper';
import headerActions from './actions';
import HeaderLayout from './components/HeaderLayout.jsx';
import HeaderLogo from './components/HeaderLogo.jsx';
import localStorageKey from '../helppers/constants';
import ShoppingCart from './components/ShoppingCart.jsx';
import './header.css';

const CategoryMenu = categoryMenu.CategoryMenu;

class Header extends Component {
  componentWillMount() {
    const cartStore = getLocalStorage(localStorageKey);
    if(cartStore) {
      this.props.headerActions.setItemQuantity(cartStore.length);
    };
  };

  render() {
    return (
      <HeaderLayout>
        <HeaderLogo />
        <CategoryMenu />
        <ShoppingCart
          itemQuantity={this.props.itemQuantity}
        />
      </HeaderLayout>
    );
  };
};

Header.propTypes = {
  itemQuantity: Proptypes.number,
}

Header.defaultProps = {
  itemQuantity: 0,
};

const mapStateToProps = () => (
  createStructuredSelector({
    itemQuantity: getItemQuantity,
  })
);

const mapDispatchToProps = (dispatch) => {
  return {
    headerActions: bindActionCreators(headerActions, dispatch),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header);
