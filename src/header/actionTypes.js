const SET_ITEM_QUANTITY = 'SET_ITEM_QUANTITY';

export default {
  SET_ITEM_QUANTITY,
};
