import React from 'react';
import AliceCarousel from 'react-alice-carousel';
import itemCard from '../itemCard';

import 'react-alice-carousel/lib/alice-carousel.css';

const ItemCard = itemCard.ItemCard;

const Carousel = (props) => {
  const responsive = {
    0: { items: 1 },
    424: { items: 2 },
    767: { items: 3 },
    1000: { items: 4 },
  };

  const galleryItems = () => (
    props.products.map((product) =>(
        <ItemCard
          {...product}
        />
    ))
  )

  return (
    <AliceCarousel
      dotsDisabled={true}
      items={galleryItems()}
      mouseDragEnabled
      responsive={responsive}
    />
  )
};

export default Carousel;
