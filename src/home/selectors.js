import constants from './constants';

const getProducts = state => state[constants.NAME].products;

export default getProducts;
