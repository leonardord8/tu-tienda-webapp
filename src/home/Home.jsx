import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import PropTypes from 'prop-types';
import carousel from '../carousel';
import getProducts from './selectors';
import HomeLayout from './components/HomeLayout.jsx';
import homeActions from './actions';
import productsClient from '../clients/productsClient';

const Carousel = carousel.Carousel;

class Home extends Component {
  async componentDidMount() {
    const products = await productsClient.getProducts();
    this.props.homeActions.setProducts(products);
  }

  randomizeProducts = () => {
    const arrayToRender = [];
    if (this.props.products.length) {
      const maxRandNumber = this.props.products.length - 4;
      const randomsPositions = Math.floor(Math.random() * Math.floor(maxRandNumber));

      for (let i = 0; i < 5; i++) {
        arrayToRender.push(this.props.products[randomsPositions + i]);
      }
    }

    return arrayToRender;
  }

  render() {
    return (
      <HomeLayout>
        <Carousel
          products={this.randomizeProducts()}
        />
      </HomeLayout>
    );
  };
};

Home.proptypes = {
  homeActions: PropTypes.object.isRequired,
};

const mapStateToProps = () => (
  createStructuredSelector({
    products: getProducts,
  })
);

const mapDispatchToProps = (dispatch) => ({
  homeActions: bindActionCreators(homeActions, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
