import { combineReducers } from 'redux';
import actionTypes from './actionTypes';

const initialHomeState = {
  products: [],
};

const setProductsReducer = (state = initialHomeState.products, action = {}) => {
  switch (action.type) {
    case actionTypes.SET_PRODUCTS:
      return action.payload;
    default:
      return state;
  }
};

export default combineReducers({
  products: setProductsReducer,
});
