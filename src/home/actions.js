import actionTypes from './actionTypes';

const setProducts = payload => ({
  type: actionTypes.SET_PRODUCTS,
  payload,
});

export default {
  setProducts,
};
