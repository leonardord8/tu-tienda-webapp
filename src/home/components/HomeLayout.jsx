import React from 'react';
import PropTypes from 'prop-types';

const HomeLayout = (props) => (
  <div
    className='body-content home-layout'
  >
    {props.children}
  </div>
);

HomeLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default HomeLayout;
