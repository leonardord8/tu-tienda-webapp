import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import CartProduct from './components/CartProduct.jsx';
import { getLocalStorage, setLocalStorage } from '../helppers/localStorageHelpper';
import header from '../header';
import home from '../home';
import localStorageKey from '../helppers/constants';
import selectors from './selectors';
import shoppingCartActions from './actions';
import ShoppingCartLayout from './components/ShoppingCartLayout.jsx';
import './shoppingCart.css'

class ShoppingCart extends Component {
  componentDidMount() {
    const shoppingCartIds = getLocalStorage(localStorageKey);
    const shoppingCart = this.getShoppingCartProducts(shoppingCartIds);

    this.props.shoppingCartActions.setShoppingCart(shoppingCart);
  };

  getShoppingCartProducts = (shoppingCartIds) => this.props.products.filter((prod) => shoppingCartIds.includes(prod.id));

  deleteCartProduct = (cartProduct) => {
    let shoppingCartIds = getLocalStorage(localStorageKey);
    shoppingCartIds = shoppingCartIds.filter((cartProductId) => cartProductId !== cartProduct.id);
    setLocalStorage(localStorageKey, shoppingCartIds);
    this.props.headerActions.setItemQuantity(shoppingCartIds.length);
    const shoppingCart = this.getShoppingCartProducts(shoppingCartIds);
    this.props.shoppingCartActions.setShoppingCart(shoppingCart);
  };

  onBuyClick = (cartProduct) => {
    const quantityToBuy = parseInt(document.getElementById(cartProduct.id).value);
    if (isNaN(quantityToBuy) || quantityToBuy >= cartProduct.quantity || Math.sign(quantityToBuy) === -1) {
      window.alert('No disponemos de la cantidad seleccionada.');
      return null;
    }

    window.alert('Producto comprado.');
    this.deleteCartProduct(cartProduct);
  };

  onDeleteClick = (cartProduct) => {
    window.alert('Producto eliminado');
    this.deleteCartProduct(cartProduct);
  };

  renderCartProducts = () => (
    this.props.shoppingCart.map((cartProduct) => (
      <CartProduct
        key={cartProduct.id}
        {...cartProduct}
        onBuyClick={this.onBuyClick}
        onDeleteClick={this.onDeleteClick}
      />
    ))
  );

  render() {
    return (
      <ShoppingCartLayout>
        {this.renderCartProducts()}
      </ShoppingCartLayout>
    );
  };
};

ShoppingCart.propTypes = {
  products: PropTypes.array.isRequired,
  shoppingCart: PropTypes.array.isRequired,
  shoppingCartActions: PropTypes.object.isRequired,
};

const mapStateToProps = () => (
  createStructuredSelector({
    products: home.selectors,
    shoppingCart: selectors,
  })
);

const mapDispatchToProps = (dispatch) => ({
  shoppingCartActions: bindActionCreators(shoppingCartActions, dispatch),
  headerActions: bindActionCreators(header.actions, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ShoppingCart);
