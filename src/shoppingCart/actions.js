import actionTypes from './actionTypes';

const setShoppingCart = payload => ({
  type: actionTypes.SET_SHOPPING_CART,
  payload,
});

export default {
  setShoppingCart,
};
