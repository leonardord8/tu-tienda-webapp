import React from 'react';
import PropTypes from 'prop-types';
import Button from '../../button/Button.jsx';

const CartProduct = (props) => (
  <article
    className='product'
  >
    <div
      className='product-description'
    >
      <h2>{props.name}</h2>
      <div
        className='button-cart-product'
      >
        <Button
          className='btn-secondary'
          label='eliminar'
          onClick={() => props.onDeleteClick(props)}
        />
        <Button
          className='btn-primary'
          label='comprar'
          onClick={() => props.onBuyClick(props)}
        />
      </div>
    </div>
    <div
      className='quantity-selector'
    >
      <input
        id={props.id}
        type='number'
        min='1'
        max={props.quantity.toString()}
      />
      <div>Cantidad disponible: {props.quantity}</div>
    </div>
    <div
      className='product-price'
    >
      <span>
        {props.price}
      </span>
    </div>
  </article>
)

CartProduct.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onDeleteClick: PropTypes.func.isRequired,
  onBuyClick: PropTypes.func.isRequired,
  quantity: PropTypes.number.isRequired,
  price: PropTypes.string.isRequired,
};

export default CartProduct;
