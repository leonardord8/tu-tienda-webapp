import React from 'react';
import PropTypes from 'prop-types';

const ShoppingCartLayout = (props) => (
  <div
    className='body-content shopping-cart-layout'
  >
    {props.children}
  </div>
);

ShoppingCartLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ShoppingCartLayout;
