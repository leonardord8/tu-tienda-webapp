import { combineReducers } from 'redux';
import actionTypes from './actionTypes';

const initialShoppingCartState = {
  shoppingCart: [],
};

const setShoppingCartReducer = (state = initialShoppingCartState.shoppingCart, action = {}) => {
  switch (action.type) {
    case actionTypes.SET_SHOPPING_CART:
      return [...action.payload];
    default:
      return state;
  }
};

export default combineReducers({
  shoppingCart: setShoppingCartReducer,
});
