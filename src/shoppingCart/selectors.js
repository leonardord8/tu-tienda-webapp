import constants from './constants';

const getShoppingCart = state => state[constants.NAME].shoppingCart;

export default getShoppingCart;
