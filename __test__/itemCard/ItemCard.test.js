import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { mount } from 'enzyme';
import { Provider } from 'react-redux';
import * as localStorageHelpper from '../../src/helppers/localStorageHelpper';
import itemCard from '../../src/itemCard';
import store from '../../src/store';

Enzyme.configure({ adapter: new Adapter() });
jest.mock('../../src/helppers/localStorageHelpper');

describe('Testing ItemCard.jsx', () => {
  const ItemCard = itemCard.ItemCard;
  let wrapper = null;
  const spyGetLocal = jest.spyOn(localStorageHelpper, 'getLocalStorage');
  const spySetLocal = jest.spyOn(localStorageHelpper, 'setLocalStorage');
  beforeEach(() => {
    wrapper = mount(
      <Provider
        store={store}
      >
        <ItemCard
          id={'id'}
          availability={true}
          price={'$9.99'}
          name={'TestName'}
        />
      </Provider>
    );
  })
  afterEach(() => {
    spyGetLocal.mockRestore();
    spySetLocal.mockRestore();
    wrapper.unmount();
  });

  it('Testing onAddItemToCart with item in cart', () => {
    localStorageHelpper.getLocalStorage.mockImplementation(() => []);
    localStorageHelpper.checkItemInCart.mockImplementation(() => true);
    wrapper.find('.btn.btn-primary').simulate('click');
    expect(spyGetLocal).toHaveBeenCalled();
    expect(spySetLocal).not.toHaveBeenCalled();
  });

  it('Testing onAddItemToCart without item in cart', () => {
    localStorageHelpper.getLocalStorage.mockImplementation(() => [{ id: 1 }, { id: 2 }, { id: 3 }]);
    localStorageHelpper.checkItemInCart.mockImplementation(() => false);
    wrapper.find('.btn.btn-primary').simulate('click');
    expect(spyGetLocal).toHaveBeenCalled();
    expect(spySetLocal).toHaveBeenCalled();
  });

  it('Testing layout', () => {
    const productName = wrapper.find('.product-name');
    expect(productName.text()).toMatch('TestName');
  });
});
