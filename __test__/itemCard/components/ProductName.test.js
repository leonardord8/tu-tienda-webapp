import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { mount } from 'enzyme';
import ProductName from '../../../src/itemCard/components/ProductName.jsx';

Enzyme.configure({ adapter: new Adapter() });

describe('Testing ProductName.jsx', () => {
  it('Testing layout available', () => {
    const wrapper = mount(
      <ProductName
        productName="Test"
      />
    );
    const text = wrapper.find('p');
    expect(text.text()).toMatch('Test');
  });
});
