import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { mount } from 'enzyme';
import ItemCardLayout from '../../../src/itemCard/components/ItemCardLayout.jsx';

Enzyme.configure({ adapter: new Adapter() });

describe('Testing ItemCardLayout.jsx', () => {
  it('Testing layout', () => {
    const wrapper = mount(
      <ItemCardLayout>
        <p>Test</p>
      </ItemCardLayout>
    );
    const text = wrapper.find('p');
    expect(text.text()).toMatch('Test');
  });
});
