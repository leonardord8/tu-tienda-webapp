import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { mount } from 'enzyme';
import ProductAvailability from '../../../src/itemCard/components/ProductAvailability.jsx';

Enzyme.configure({ adapter: new Adapter() });

describe('Testing ProductAvailability.jsx', () => {
  it('Testing layout available', () => {
    const wrapper = mount(
      <ProductAvailability
        availability
      />
    );
    const text = wrapper.find('p').first();
    expect(text.text()).toMatch('Disponible');
  });
  it('Testing layout available', () => {
    const wrapper = mount(
      <ProductAvailability
        availability={false}
      />
    );
    const text = wrapper.find('p').first();
    expect(text.text()).toMatch('Sin stock');
  });
});
