import axios from 'axios';
import productsClient from '../../src/clients/productsClient';
import * as productStub from '../../stubby/stubbyMocks/products.json';

jest.mock('axios');

describe('Testing productsClient', () => {
  it('testing getProducts', async () => {
    const stubbys = {
      data: {
        products: productStub.products.slice(0, 5)
      }
    };

    axios.get.mockResolvedValue(stubbys);
    const response = await productsClient.getProducts();
    expect(response).toEqual(productStub.products.slice(0, 5));
  });
  it('testing getCategories', async () => {
    axios.get.mockRejectedValue(new Error('Error test'));
    const response = await productsClient.getProducts();
    expect(response).toEqual([]);
  });
});
