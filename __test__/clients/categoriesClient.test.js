import axios from 'axios';
import categoriesClient from '../../src/clients/categoriesClient';
import * as categoryStub from '../../stubby/stubbyMocks/categories.json';

jest.mock('axios');

describe('Testing categoriesClient', () => {
  it('testing getCategories', async () => {
    const stubbys = {
      data: {
        categories: categoryStub.categories.slice(0, 5)
      }
    };

    axios.get.mockResolvedValue(stubbys);
    const response = await categoriesClient.getCategories();
    expect(response).toEqual(categoryStub.categories.slice(0, 5));
  });

  it('testing getCategories', async () => {
    axios.get.mockRejectedValue(new Error('Error test'));
    const response = await categoriesClient.getCategories();
    expect(response).toEqual([]);
  });
});
