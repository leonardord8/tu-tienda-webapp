import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { mount } from 'enzyme';
import HomeLayout from '../../../src/home/components/HomeLayout.jsx';

Enzyme.configure({ adapter: new Adapter() });

describe('Testing HomeLayout.jsx', () => {
  it('Testing layout', () => {
    const wrapper = mount(
      <HomeLayout>
        <p>Test</p>
      </HomeLayout>
    );
    const text = wrapper.find('p');
    expect(text.text()).toMatch('Test');
  });
});
