import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { BrowserRouter as Router } from 'react-router-dom';
import Enzyme, { mount } from 'enzyme';
import { Provider } from 'react-redux';
import home from '../../src/home';
import productsClient from '../../src/clients/productsClient';
import store from '../../src/store';
import * as productStub from '../../stubby/stubbyMocks/products.json';

Enzyme.configure({ adapter: new Adapter() });
jest.mock('../../src/clients/productsClient');

describe('Testing Home.jsx', () => {
  beforeEach(() => jest.resetAllMocks());
  it('Testing layout', () => {
    productsClient.getProducts.mockResolvedValue(productStub.products.slice(0, 7))
    const Home = home.Home;
    const wrapper = mount(
      <Provider
        store={store}
      >
        <Router>
          <Home />
        </Router>
      </Provider>
    );
    expect(productsClient.getProducts).toBeCalled();
    const layout = wrapper.find('.home-layout');
    expect(layout).toHaveLength(1);
  });
});
