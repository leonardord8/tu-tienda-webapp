import home from '../../src/home';
const homeReducers = home.reducers;
const homeActionTypes = home.actionTypes;

describe('Testing home reducers', () => {
  it('Testing initial state', () => {
    expect(homeReducers(undefined, {})).toEqual({
      products: [],
    });
  });

  it('Testing setProductsReducer', () => {
    const payloadObject = [{ id: 1 }, { id: 2 }];
    expect(homeReducers(undefined, {
      type: homeActionTypes.SET_PRODUCTS,
      payload: payloadObject,
    })).toEqual({
      products: payloadObject,
    });
  });
});
