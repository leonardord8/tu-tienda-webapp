import home from '../../src/home';
const homeSelectors = home.selectors;

describe('Testing home selectors', () => {
  it('Testing getItemQuantity', () => {
    expect(homeSelectors({ 'home': { 'products': [] } })).toEqual([]);
  });
});
