import home from '../../src/home';
const homeActions = home.actions;
const homeActionTypes = home.actionTypes;

describe('Testing the home actions', () => {
  it('Testing setProducts', () => {
    const payloadObject = [{ id: 1 }, { id: 2 }];
    const products = homeActions.setProducts(payloadObject);
    expect(products).toEqual({
      type: homeActionTypes.SET_PRODUCTS,
      payload: payloadObject,
    });
  });
});
