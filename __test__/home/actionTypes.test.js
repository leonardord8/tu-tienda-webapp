import home from '../../src/home';
const homeActionTypes = home.actionTypes;

describe('Tesing action types', () => {
  it('testing SET_PRODUCTS const', () => {
    expect(homeActionTypes.SET_PRODUCTS).toMatch('SET_PRODUCTS');
  });

  it('testing action types length', () => {
    expect(Object.keys(homeActionTypes).length).toBe(1);
  });
});
