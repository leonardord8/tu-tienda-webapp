import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { BrowserRouter as Router } from 'react-router-dom';
import Enzyme, { mount } from 'enzyme';
import { Provider } from 'react-redux';
import { getLocalStorage } from '../../src/helppers/localStorageHelpper';
import header from '../../src/header';
import store from '../../src/store';

Enzyme.configure({ adapter: new Adapter() });
jest.mock('../../src/helppers/localStorageHelpper');

describe('Testing Header.jsx', () => {
  const Header = header.Header;
  let wrapper = null;
  beforeEach(() => {
    wrapper = mount(
      <Provider
        store={store}
      >
        <Router>
          <Header />
        </Router>
      </Provider>
    );
    jest.resetAllMocks();
  });

  it('Testing layout', () => {
    const div = wrapper.find('.page-header');
    expect(div).toHaveLength(1);
  });

  it('Testing default item quantity', () => {
    getLocalStorage.mockImplementation(() => undefined);
    const span = wrapper.find('.badge.badge-default');
    expect(span.text()).toMatch('0');
  });

  it('Testing get from local storage', () => {
    getLocalStorage.mockImplementation(() => [{ id: 1 }, { id: 2 }, { id: 3 }]);
    wrapper = mount(
      <Provider
        store={store}
      >
        <Router>
          <Header />
        </Router>
      </Provider>
    );
    const span = wrapper.find('.badge.badge-default');
    expect(span.text()).toMatch('3');
  });
});
