import header from '../../src/header';
const headerSelectors = header.selectors;

describe('Testing header selectors', () => {
  it('Testing getItemQuantity', () => {
    expect(headerSelectors({'header': {'itemQuantity': 0}})).toBe(0);
  });
});
