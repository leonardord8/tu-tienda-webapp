import header from '../../src/header';
const headerReducers = header.reducers;
const headerActionTypes = header.actionTypes;

describe('Testing header reducers', () => {
  it('Testing initial state', () => {
    expect(headerReducers(undefined, {})).toEqual({
      itemQuantity: 0,
    });
  });

  it('Testing setItemQuantityReducer', () => {
    expect(headerReducers(undefined, {
      type: headerActionTypes.SET_ITEM_QUANTITY,
      payload: 3
    })).toEqual({
      itemQuantity: 3,
    });
  });
});
