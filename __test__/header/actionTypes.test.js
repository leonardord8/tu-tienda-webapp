import header from '../../src/header';
const headerActionTypes = header.actionTypes;

describe('Tesing action types', () => {
  it('testing SET_ITEM_QUANTITY const', () => {
    expect(headerActionTypes.SET_ITEM_QUANTITY).toMatch('SET_ITEM_QUANTITY');
  });

  it('testing action types length', () => {
    expect(Object.keys(headerActionTypes).length).toBe(1);
  });
});
