import header from '../../src/header';
const headerActions = header.actions;
const headerActionTypes = header.actionTypes;

describe('Testing the header actions', () => {
  it('Testing setItemQuantity', () => {
    const itemQuantitySetted = headerActions.setItemQuantity(2);
    expect(itemQuantitySetted).toEqual({
      type: headerActionTypes.SET_ITEM_QUANTITY,
      payload: 2
    });
  });
});
