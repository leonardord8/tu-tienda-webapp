import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { BrowserRouter as Router } from 'react-router-dom';
import HeaderLogo from '../../../src/header/components/HeaderLogo.jsx';

Enzyme.configure({ adapter: new Adapter() });

describe('Testing HeaderLogo.jsx', () => {
  let wrapper = null;
  beforeEach(() => {
    wrapper = mount(
      <Router>
        <HeaderLogo />
      </Router>
    );
  });
  it('Testing layout', () => {
    const div = wrapper.find('.page-logo');
    expect(div).toHaveLength(1);
    const div2 = wrapper.find('.default-logo');
    expect(div2).toHaveLength(1);
  });

  it('Testing redirect', () => {
    wrapper.find('a').simulate('click');
    expect(document.location.href).toBe('http://localhost/');
  })
});
