import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { BrowserRouter as Router } from 'react-router-dom';
import ShoppingCart from '../../../src/header/components/ShoppingCart.jsx';

Enzyme.configure({ adapter: new Adapter() });

describe('Testing ShoppingCart.jsx', () => {
  it('Testing layout', () => {
    const wrapper = mount(
      <Router>
        <ShoppingCart />
      </Router>
    );
    const div = wrapper.find('.shopping-cart-header');
    expect(div).toHaveLength(1);
    const a = wrapper.find('a.my-cart-link');
    expect(a).toHaveLength(1);
  });
});
