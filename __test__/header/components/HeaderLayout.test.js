import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { BrowserRouter as Router } from 'react-router-dom';
import HeaderLayout from '../../../src/header/components/HeaderLayout.jsx';

Enzyme.configure({ adapter: new Adapter() });

describe('Testing HeaderLayout.jsx', () => {
  it('Testing layout', () => {
    const wrapper = mount(
      <Router>
        <HeaderLayout>
          <div
            className="test-class"
          />
        </HeaderLayout>
      </Router>
    );
    const div = wrapper.find('.test-class');
    expect(div).toHaveLength(1);
    const div2 = wrapper.find('.page-header');
    expect(div2).toHaveLength(1);
  });
});
