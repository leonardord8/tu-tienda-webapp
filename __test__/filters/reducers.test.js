import filters from '../../src/filters';
const filtersReducers = filters.reducers;
const filtersActionTypes = filters.actionTypes;

describe('Testing filters reducers', () => {
  it('Testing the setSelectedFiltersReducer', () => {
    const payloadObject = [{
      id: 1,
      name: 'Disponibilidad',
      fieldToCompare: 'availability',
      isAGroup: false,
      groupValues: null,
    },];
    expect(filtersReducers(undefined,
    {
      type: filtersActionTypes.SET_SELECTED_FILTERS,
      payload: payloadObject
    })).toEqual({
      existingFilters: payloadObject,
      selectedFilters: payloadObject
    });
  });
});
