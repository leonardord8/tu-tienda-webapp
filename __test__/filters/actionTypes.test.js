import filters from '../../src/filters';
const filtersActionTypes = filters.actionTypes;

describe('Tesing action types', () => {
  it('testing SET_SELECTED_FILTERS const', () => {
    expect(filtersActionTypes.SET_SELECTED_FILTERS).toMatch('SET_SELECTED_FILTERS');
  });

  it('testing action types length', () => {
    expect(Object.keys(filtersActionTypes).length).toBe(1);
  });
});
