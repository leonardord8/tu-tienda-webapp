import filtersHelppers from '../../../src/filters/filterHelppers/filterHelppers';
import * as productStub from '../../../stubby/stubbyMocks/products.json';

describe('Testing filter helpper', () => {
  it('Testing addFilter is not a group', () => {
    const returnedArray = filtersHelppers.addFilter(
      {
        id: 1,
        name: 'Disponibilidad',
        fieldToCompare: 'availability',
        isAGroup: false,
        groupValues: null,
      },
      []
    );

    expect(returnedArray).toHaveLength(1);
  });

  it('Testing addFilter is a group not listed', () => {
    const returnedArray = filtersHelppers.addFilter(
      {
        id: 1,
        name: 'Disponibilidad',
        fieldToCompare: 'availability',
        isAGroup: true,
        groupValues: [
          'test1',
          'test2'
        ],
        value: [
          'test1',
        ],
      },
      []
    );

    expect(returnedArray).toHaveLength(1);
    expect(returnedArray[0].value).toHaveLength(1);
  });

  it('Testing addFilter is a group already listed', () => {
    const returnedArray = filtersHelppers.addFilter(
      {
        id: 1,
        name: 'Disponibilidad',
        fieldToCompare: 'availability',
        isAGroup: true,
        groupValues: [
          'test1',
          'test2',
        ],
        value: [
          'test2',
        ],
      },
      [{
        id: 1,
        name: 'Disponibilidad',
        fieldToCompare: 'availability',
        isAGroup: true,
        groupValues: [
          'test1',
          'test2',
        ],
        value: [
          'test1',
        ],
      }]
    );

    expect(returnedArray).toHaveLength(1);
    expect(returnedArray[0].value).toHaveLength(2);
  });

  it('Testing deleteFilter is not a group', () => {
    const returnedArray = filtersHelppers.deleteFilter({
      id: 1,
      name: 'Disponibilidad',
      fieldToCompare: 'availability',
      isAGroup: false,
      groupValues: null,
    }, [{
      id: 1,
      name: 'Disponibilidad',
      fieldToCompare: 'availability',
      isAGroup: false,
      groupValues: null,
    }]);

    expect(returnedArray).toHaveLength(0);
  });

  it('Testing deleteFilter is not a group', () => {
    const returnedArray = filtersHelppers.deleteFilter({
      id: 1,
      name: 'Disponibilidad',
      fieldToCompare: 'availability',
      isAGroup: false,
      groupValues: null,
    }, [{
      id: 1,
      name: 'Disponibilidad',
      fieldToCompare: 'availability',
      isAGroup: false,
      groupValues: null,
    }]);

    expect(returnedArray).toHaveLength(0);
  });

  it('Testing deleteFilter is not a group', () => {
    const returnedArray = filtersHelppers.deleteFilter({
      id: 1,
      name: 'Disponibilidad',
      fieldToCompare: 'availability',
      isAGroup: true,
      groupValues: [
        'test1',
        'test2',
      ],
      value: [
        'test2',
      ],
    },
      [{
        id: 1,
        name: 'Disponibilidad',
        fieldToCompare: 'availability',
        isAGroup: true,
        groupValues: [
          'test1',
          'test2',
        ],
        value: [
          'test2',
        ]
      }]
    );

    expect(returnedArray).toHaveLength(1);
    expect(returnedArray[0].value).toHaveLength(0);
  });

  it('Testing getFilteredAndOrderedList with a filter not group', () => {
    const returnedArray = filtersHelppers.getFilteredAndOrderedList(
      1,
      productStub.products,
      [{
        id: 1,
        name: 'Disponibilidad',
        fieldToCompare: 'availability',
        isAGroup: false,
        groupValues: null,
      }],
      'null'
    );

    expect(returnedArray).toHaveLength(4);
    expect(returnedArray[1].availability).toBeTruthy();
    expect(returnedArray[2].sublevel_id).toBe(1);
  });

  it('Testing getFilteredAndOrderedList with a filter group', () => {
    const returnedArray = filtersHelppers.getFilteredAndOrderedList(
      3,
      productStub.products,
      [{
        id: 1,
        name: 'Nombre',
        fieldToCompare: 'name',
        isAGroup: true,
        groupValues: [
          'mollit',
          'Test1',
        ],
        value: [
          'mollit',
        ]
      }],
      'null'
    );

    expect(returnedArray).toHaveLength(1);
    expect(returnedArray[0].name).toMatch('mollit');
    expect(returnedArray[0].sublevel_id).toBe(3);
  });
});
