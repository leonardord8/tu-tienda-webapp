import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { mount } from 'enzyme';
import FiltersLayout from '../../../src/filters/components/FiltersLayout.jsx';

Enzyme.configure({ adapter: new Adapter() });

describe('Testing FiltersLayout.jsx', () => {
  it('Testing layout', () => {
    const wrapper = mount(
      <FiltersLayout>
        <p>Test</p>
      </FiltersLayout>
    );
    const text = wrapper.find('p');
    expect(text.text()).toMatch('Test');
    const aside = wrapper.find('.filters-layout');
    expect(aside).toHaveLength(1);
  });
});
