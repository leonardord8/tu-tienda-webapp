import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { mount } from 'enzyme';
import Filter from '../../../src/filters/components/Filter.jsx';

Enzyme.configure({ adapter: new Adapter() });

describe('Testing Filter.jsx', () => {
  it('Testing layout is not a group', () => {
    const wrapper = mount(
      <Filter
        {
          ...{
            id: 1,
            name: 'Test',
            fieldToCompare: 'availability',
            isAGroup: false,
            groupValues: null,
            onClick: () => {},
          }
        }
      />
    );
    const text = wrapper.find('span');
    expect(text.text()).toMatch('Test');
    expect(text).toHaveLength(1);
  });

  it('Testing layout is a group', () => {
    const wrapper = mount(
      <Filter
        {
          ...{
            id: 1,
            name: 'Test',
            fieldToCompare: 'availability',
            isAGroup: true,
            groupValues: ['test1', 'test2'],
            onClick: () => {},
          }
        }
      />
    );
    const text = wrapper.find('span').first();
    expect(text.text()).toMatch('Test');
    const text1 = wrapper.find('span').at(1);
    expect(text1.text()).toMatch('test1');
    const text2 = wrapper.find('span').at(2);
    expect(text2.text()).toMatch('test2');
  });

  it('Testing layout is not a group click', () => {
    const mockfn = jest.fn()
    const wrapper = mount(
      <Filter
        {
          ...{
            id: 1,
            name: 'Test',
            fieldToCompare: 'availability',
            isAGroup: false,
            groupValues: null,
            onClick: mockfn,
          }
        }
      />
    );
    const text = wrapper.find('span');
    expect(text.text()).toMatch('Test');
    expect(text).toHaveLength(1);

    const input = wrapper.find('input');
    input.simulate('click');
    expect(mockfn).toHaveBeenCalled();
  });

  it('Testing layout is a group click', () => {
    const mockfn = jest.fn()
    const wrapper = mount(
      <Filter
        {
          ...{
            id: 1,
            name: 'Test',
            fieldToCompare: 'availability',
            isAGroup: true,
            groupValues: ['test1', 'test2'],
            onClick: mockfn,
          }
        }
      />
    );
    const text = wrapper.find('span').first();
    expect(text.text()).toMatch('Test');
    const text1 = wrapper.find('span').at(1);
    expect(text1.text()).toMatch('test1');
    const text2 = wrapper.find('span').at(2);
    expect(text2.text()).toMatch('test2');

    const input = wrapper.find('input').first();
    input.simulate('click');
    expect(mockfn).toHaveBeenCalled();
  });
});
