import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { createStructuredSelector } from 'reselect';
import Enzyme, { mount } from 'enzyme';
import { Provider } from 'react-redux';
import filters from '../../src/filters';
import store from '../../src/store';

Enzyme.configure({ adapter: new Adapter() });
jest.mock('reselect');
jest.mock('../../src/filters/actions')

describe('Testing Filters.jsx', () => {
  beforeEach(() => jest.resetAllMocks());
  it('Testing layout', () => {
    const mock = filters.actions.setSelectedFilters.mockReturnValue({
      type: filters.actionTypes.SET_SELECTED_FILTERS,
      payload: [],
    });

    createStructuredSelector.mockReturnValue({
      selectedFilters: [],
      existingFilters: [
        {
          id: 1,
          name: 'Disponibilidad',
          fieldToCompare: 'availability',
          isAGroup: false,
          groupValues: null,
        },
      ],
    });
    const Filters = filters.Filters;
    const wrapper = mount(
      <Provider
        store={store}
      >
        <Filters />
      </Provider>
    );

    const layout = wrapper.find('.filters-layout');
    expect(layout).toHaveLength(1);
    const input = wrapper.find('input');
    expect(input).toHaveLength(1);
    input.simulate('click');
    expect(mock).toHaveBeenCalled();
  });
});
