import filters from '../../src/filters';
const filtersActions = filters.actions;
const filtersActionTypes = filters.actionTypes;

describe('Testing the filters actions', () => {
  it('Testing setSelectedFilters', () => {
    const payloadObject = [{
      id: 1,
      name: 'Disponibilidad',
      fieldToCompare: 'availability',
      isAGroup: false,
      groupValues: null,
    }];
    const products = filtersActions.setSelectedFilters(payloadObject);
    expect(products).toEqual({
      type: filtersActionTypes.SET_SELECTED_FILTERS,
      payload: payloadObject,
    });
  });
});
