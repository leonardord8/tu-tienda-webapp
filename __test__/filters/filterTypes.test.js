import filterTypes from '../../src/filters/filterTypes';

describe('Testing filterTypes', () => {
  it('Testing filterTypes array', () => {
    expect(filterTypes).toHaveLength(1);
  });
});

