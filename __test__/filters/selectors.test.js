import filters from '../../src/filters';
const filtersSelectors = filters.selectors;

describe('Testing filters selectors', () => {
  it('Testing getExistingFilters', () => {
    expect(filtersSelectors.getExistingFilters(
      {
        filters: {
          existingFilters: { 
            id: 1,
            name: 'Disponibilidad',
            fieldToCompare: 'availability',
            isAGroup: false,
            groupValues: null,
          }
        }
      }
    )).toEqual(
      { 
        id: 1,
        name: 'Disponibilidad',
        fieldToCompare: 'availability',
        isAGroup: false,
        groupValues: null,
      }
    );
  });

  it('Testing getSelectedFilters', () => {
    expect(filtersSelectors.getSelectedFilters(
      {
        filters: {
          selectedFilters: { 
            id: 1,
            name: 'Disponibilidad',
            fieldToCompare: 'availability',
            isAGroup: false,
            groupValues: null,
          }
        }
      }
    )).toEqual(
      { 
        id: 1,
        name: 'Disponibilidad',
        fieldToCompare: 'availability',
        isAGroup: false,
        groupValues: null,
      }
    );
  });
});
