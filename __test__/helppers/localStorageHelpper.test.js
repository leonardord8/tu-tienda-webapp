import { getLocalStorage, setLocalStorage, checkItemInCart } from '../../src/helppers/localStorageHelpper';
import { localStorageKey } from '../../src/helppers/constants';


describe('Testing localstorage helpper', () => {
  it('Testing getLocalStorage calls localStorage.getItem', () => {
    const spy = jest.spyOn(Storage.prototype, 'getItem');
    getLocalStorage(localStorageKey);
    expect(spy).toHaveBeenCalled();
  });

  it('Testing setLocalStorage calls localStorage.setItem', () => {
    const spy = jest.spyOn(Storage.prototype, 'setItem');
    setLocalStorage(localStorageKey, 'lorem');
    expect(spy).toHaveBeenCalled();
  });

  it('Testing setLocalStorage calls localStorage.checkItemInCart returns true', () => {
    expect(checkItemInCart('id1', ['id1'])).toBeTruthy();
  });

  it('Testing setLocalStorage calls localStorage.checkItemInCart returns false', () => {
    expect(checkItemInCart('id1', ['id2'])).toBeFalsy();
  });
});
