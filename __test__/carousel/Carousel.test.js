import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { mount } from 'enzyme';
import { Provider } from 'react-redux';
import carousel from '../../src/carousel';
import * as productJson from '../../stubby/stubbyMocks/products.json';
import store from '../../src/store';

Enzyme.configure({ adapter: new Adapter() });
const products = productJson.products.slice(0,5);
describe('Testing Carousel.jsx', () => {
  const Carousel = carousel.Carousel;
  it('Testing layout', () => {
    const wrapper = mount(
      <Provider
        store={store}
      >
        <Carousel
          products={products}
        />
      </Provider>
    )
    const div = wrapper.find('.alice-carousel');
    expect(div).toHaveLength(1);
  });
});
