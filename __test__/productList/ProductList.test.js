import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import * as reselect from 'reselect';
import Enzyme, { mount } from 'enzyme';
import { Provider } from 'react-redux';
import selectors from '../../src/filters/selectors';
import productList from '../../src/productList';
import store from '../../src/store';
import * as productStub from '../../stubby/stubbyMocks/products.json';

Enzyme.configure({ adapter: new Adapter() });
jest.mock('reselect');
 
describe('Testing ProductList.jsx', () => {
  const ProductList = productList.ProductList;
  beforeEach(() => jest.resetAllMocks());
  
  it('Testing onAddItemToCart without an item in cart', () => {
    reselect.createSelector.mockReturnValueOnce({ productList: []});
    reselect.createStructuredSelector.mockReturnValueOnce({
      selectedFilters: [],
      existingFilters: [
        {
          id: 1,
          name: 'Disponibilidad',
          fieldToCompare: 'availability',
          isAGroup: false,
          groupValues: null,
        },
      ],
    })
    let wrapper = mount(
      <Provider
        store={store}
      >
        <ProductList
          match={{
            params: {
              categoryId: 1,
            }
          }}
        />
      </Provider>
    );

    const div = wrapper.find('.body-content');
    expect(div).toHaveLength(1);
    const h2 = wrapper.find('h2');
    expect(h2.text()).toMatch('No se encuentran productos para esta categoria.');
  });

  it('Testing onAddItemToCart with item in cart', () => {
    reselect.createSelector.mockReturnValueOnce({ productList: productStub.products.slice(0, 5) });
    reselect.createStructuredSelector.mockReturnValueOnce({
      selectedFilters: [],
      existingFilters: [
        {
          id: 1,
          name: 'Disponibilidad',
          fieldToCompare: 'availability',
          isAGroup: false,
          groupValues: null,
        },
      ],
    });
    let wrapper = mount(
      <Provider
        store={store}
      >
        <ProductList
          match={{
            params: {
              categoryId: 1,
            }
          }}
        />
      </Provider>
    );

    const div = wrapper.find('.body-content');
    expect(div).toHaveLength(1);
    const itemCard = wrapper.find('.item-card-layout');
    expect(itemCard).toHaveLength(5);
  });
});
