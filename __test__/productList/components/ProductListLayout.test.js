import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { mount } from 'enzyme';
import ProductListLayout from '../../../src/productList/components/ProductListLayout.jsx';

Enzyme.configure({ adapter: new Adapter() });

describe('Testing ProductListLayout.jsx', () => {
  it('Testing layout', () => {
    const wrapper = mount(
      <ProductListLayout>
        <p>Test</p>
      </ProductListLayout>
    );
    const text = wrapper.find('p');
    expect(text.text()).toMatch('Test');
    const div = wrapper.find('.body-content.product-list');
    expect(div).toHaveLength(1);
  });
});
