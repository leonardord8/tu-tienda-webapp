import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { mount } from 'enzyme';
import EmptyProductList from '../../../src/productList/components/EmptyProductList.jsx';

Enzyme.configure({ adapter: new Adapter() });

describe('Testing EmptyProductList.jsx', () => {
  it('Testing layout', () => {
    const wrapper = mount(
      <EmptyProductList />
    );
    const text = wrapper.find('h2');
    expect(text.text()).toMatch('No se encuentran productos para esta categoria.');
  });
});
