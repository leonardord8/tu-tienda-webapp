import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { mount } from 'enzyme';
import { Provider } from 'react-redux';
import sortter from '../../src/sortter';
import store from '../../src/store';

Enzyme.configure({ adapter: new Adapter() });
jest.mock('../../src/sortter/actions');

describe('Testing sortter.jsx', () => {
  const Sortter = sortter.Sortter;
  let wrapper = null;
  beforeEach(() => {
    wrapper = mount(
      <Provider
        store={store}
      >
        <Sortter />
      </Provider>
    );
  })
  it('Testing layout', () => {
    const layout = wrapper.find('.select-sortter');
    expect(layout).toHaveLength(1);
  });

  it('Testing select change', () => {
    sortter.actions.setSortterCritteria.mockReturnValue({
      type: sortter.actionTypes.SET_SORTTER_CRITTERIA,
      payload: 'null'
    })
    const select = wrapper.find('select');
    select.simulate('change');
    expect(sortter.actions.setSortterCritteria).toHaveBeenCalled();
  });
});
