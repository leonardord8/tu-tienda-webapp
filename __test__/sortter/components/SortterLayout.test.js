import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { mount } from 'enzyme';
import SortterLayout from '../../../src/sortter/components/SortterLayout.jsx';

Enzyme.configure({ adapter: new Adapter() });

describe('Testing SortterLayout.jsx', () => {
  it('Testing layout', () => {
    const mock = jest.fn();
    const wrapper = mount(
      <SortterLayout
        onChange={mock}
      >
        <option value='test'>Test</option>
      </SortterLayout>
    );
    const option = wrapper.find('option');
    expect(option.text()).toMatch('Test');
    const span = wrapper.find('span');
    expect(span.text()).toMatch('Ordenar por:');
    const select = wrapper.find('select');
    select.simulate('change');
    expect(mock).toHaveBeenCalled();
  });
});
