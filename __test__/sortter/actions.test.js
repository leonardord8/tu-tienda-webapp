import sortter from '../../src/sortter';
const sortterActions = sortter.actions;
const sortterActionTypes = sortter.actionTypes;

describe('Testing the sortter actions', () => {
  it('Testing setSortterCritteria', () => {
    const sortterCritteria = sortterActions.setSortterCritteria('null');
    expect(sortterCritteria).toEqual({
      type: sortterActionTypes.SET_SORTTER_CRITTERIA,
      payload: 'null',
    });
  });
});
