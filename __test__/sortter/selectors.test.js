import sortter from '../../src/sortter';
const sortterSelectors = sortter.selectors;

describe('Testing sortter selectors', () => {
  it('Testing getSortterCritteria', () => {
    expect(sortterSelectors({ 'sortter': { 'sortterCritteria': 'null' } })).toMatch('');
  });
});
