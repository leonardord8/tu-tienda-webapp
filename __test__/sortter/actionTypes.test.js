import sortter from '../../src/sortter';
const sortterActionTypes = sortter.actionTypes;

describe('Tesing action types', () => {
  it('testing SET_SORTTER_CRITTERIA const', () => {
    expect(sortterActionTypes.SET_SORTTER_CRITTERIA).toMatch('SET_SORTTER_CRITTERIA');
  });

  it('testing action types length', () => {
    expect(Object.keys(sortterActionTypes).length).toBe(1);
  });
});
