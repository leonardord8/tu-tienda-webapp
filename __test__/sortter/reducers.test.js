import sortter from '../../src/sortter';
const sortterReducers = sortter.reducers;
const sortterActionTypes = sortter.actionTypes;

describe('Testing sortter reducers', () => {
  it('Testing initial state', () => {
    expect(sortterReducers(undefined, {})).toEqual({
      sortterCritteria: null,
    });
  });

  it('Testing setProductsReducer', () => {
    expect(sortterReducers(undefined, {
      type: sortterActionTypes.SET_SORTTER_CRITTERIA,
      payload: 'upward',
    })).toEqual({
      sortterCritteria: 'upward',
    });
  });
});
