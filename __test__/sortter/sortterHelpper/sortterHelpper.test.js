import sortterHelpper from '../../../src/sortter/sortterHelpper/sortterHelpper';
import * as productStub from '../../../stubby/stubbyMocks/products.json';

describe('Testing sortterHelpper', () => {
  const product = productStub.products;
  it('Testing sortArray without critteria', () => {
    let arrayToCompare = sortterHelpper.sortArray(product, 'null');

    expect(arrayToCompare[0].price).toMatch('$18,332');
  });

  it('Testing sortArray upward by price', () => {
    let arrayToCompare = sortterHelpper.sortArray(product, 'upward-price');

    expect(arrayToCompare[0].price).toMatch('$1,482');
  });

  it('Testing sortArray backward by price', () => {
    let arrayToCompare = sortterHelpper.sortArray(product, 'backward-price');

    expect(arrayToCompare[0].price).toMatch('$18,332');
  });

  it('Testing sortArray upward by quantity', () => {
    let arrayToCompare = sortterHelpper.sortArray(product, 'upward-quantity');

    expect(arrayToCompare[0].quantity).toEqual(102);
  });

  it('Testing sortArray backward by quantity', () => {
    let arrayToCompare = sortterHelpper.sortArray(product, 'backward-quantity');

    expect(arrayToCompare[0].quantity).toEqual(994);
  });
});
