import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { mount } from 'enzyme';
import Button from '../../src/button/Button.jsx';

Enzyme.configure({ adapter: new Adapter() });

describe('Testing Home.jsx', () => {
  it('Testing layout', () => {
    const wrapper = mount(
      <Button
        onClick={() => {}}
      />
    );
    const btn = wrapper.find('.btn');
    expect(btn).toHaveLength(1);
  });
});
