import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { BrowserRouter as Router } from 'react-router-dom';
import Enzyme, { mount } from 'enzyme';
import { Provider } from 'react-redux';
import App from '../src/App.jsx';
import store from '../src/store';

Enzyme.configure({ adapter: new Adapter() });

describe('Testing App.jsx', () => {
  it('Testing App.jsx div render', () => {
    const wrapper = mount(
      <Provider
        store={store}
      >
        <Router>
          <App />
        </Router>
      </Provider>
    );
    const div = wrapper.find('.page-wrapper');
    expect(div).toHaveLength(1);
  });
});
