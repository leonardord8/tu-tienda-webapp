import shoppingCart from '../../src/shoppingCart';
const shoppingCartReducers = shoppingCart.reducers;
const shoppingCartActionTypes = shoppingCart.actionTypes;

describe('Testing shoppingCart reducers', () => {
  it('Testing initial state', () => {
    expect(shoppingCartReducers(undefined, {})).toEqual({
      shoppingCart: [],
    });
  });

  it('Testing setShoppingCartReducer', () => {
    const payloadObject = [{ id: 1 }, { id: 2 }];
    expect(shoppingCartReducers(undefined, {
      type: shoppingCartActionTypes.SET_SHOPPING_CART,
      payload: payloadObject,
    })).toEqual({
      shoppingCart: payloadObject,
    });
  });
});
