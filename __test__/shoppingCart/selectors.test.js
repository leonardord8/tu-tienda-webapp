import shoppingCart from '../../src/shoppingCart';
const shoppingCartSelectors = shoppingCart.selectors;

describe('Testing shoppingCart selectors', () => {
  it('Testing getShoppingCart', () => {
    expect(shoppingCartSelectors({ 'shoppingCart': { 'shoppingCart': [] } })).toEqual([]);
  });
});
