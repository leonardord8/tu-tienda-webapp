import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { mount } from 'enzyme';
import ShoppingCartLayout from '../../../src/shoppingCart/components/ShoppingCartLayout.jsx';

Enzyme.configure({ adapter: new Adapter() });

describe('Testing ShoppingCartLayout.jsx', () => {
  it('Testing layout', () => {
    const wrapper = mount(
      <ShoppingCartLayout>
        <p>Test</p>
      </ShoppingCartLayout>
    );
    const text = wrapper.find('p');
    expect(text.text()).toMatch('Test');
  });
});
