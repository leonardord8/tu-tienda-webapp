import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import Enzyme, { mount } from 'enzyme';
import CartProduct from '../../../src/shoppingCart/components/CartProduct.jsx';

Enzyme.configure({ adapter: new Adapter() });

describe('Testing CartProduct', () => {
  let wrapper = null;
  const onDelete = jest.fn();
  const onBuy = jest.fn();
  beforeEach(() => {
    wrapper = mount(
      <CartProduct
        id='test1'
        name='testName'
        onDeleteClick={onDelete}
        onBuyClick={onBuy}
        quantity={100}
        price={'$5.10'}
      />
    )
  });

  it('Testing layout', () => {
    const art = wrapper.find('article.product');
    expect(art).toHaveLength(1);
  });

  it('Testing buy click', () => {
    const buyBtn = wrapper.find('button.btn-primary');
    buyBtn.simulate('click');
    expect(onBuy).toHaveBeenCalled();
  });

  it('Testing delete click', () => {
    const buyBtn = wrapper.find('button.btn-secondary');
    buyBtn.simulate('click');
    expect(onDelete).toHaveBeenCalled();
  });
});

