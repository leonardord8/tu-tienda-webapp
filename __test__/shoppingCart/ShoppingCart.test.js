import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import * as reselect from 'reselect';
import Enzyme, { mount } from 'enzyme';
import { Provider } from 'react-redux';
import * as localStorageHelpper from '../../src/helppers/localStorageHelpper';
import * as productStub from '../../stubby/stubbyMocks/products.json';
import shoppingCart from '../../src/shoppingCart';
import store from '../../src/store';

Enzyme.configure({ adapter: new Adapter() });
jest.mock('../../src/helppers/localStorageHelpper');
jest.mock('reselect');

describe('Testing shoppingCart.jsx', () => {
  const products = productStub.products;
  const ShoppingCart = shoppingCart.ShoppingCart;
  let wrapper = null;

  beforeEach(() => jest.resetAllMocks());

  it('Testing layout', () => {
    localStorageHelpper.getLocalStorage.mockReturnValue([
      products[0].id,
      products[1].id,
      products[2].id,
    ]);
    reselect.createStructuredSelector.mockReturnValue({
      products: products,
      shoppingCart: products.slice(0, 3),
    })
    wrapper = mount(
      <Provider
      store={store}
      >
        <ShoppingCart />
      </Provider>
    );
    const layout = wrapper.find('.shopping-cart-layout');
    expect(layout).toHaveLength(1);
    expect(localStorageHelpper.getLocalStorage).toHaveBeenCalled();
  });
  
  it('Testing testing buy click', () => {
    global.document.getElementById = jest.fn().mockReturnValue({value:1});
    localStorageHelpper.getLocalStorage.mockReturnValue([
      products[0].id,
      products[1].id,
      products[2].id,
    ]);
    reselect.createStructuredSelector.mockReturnValue({
      products: products,
      shoppingCart: products.slice(0, 3),
    })
    wrapper = mount(
      <Provider
        store={store}
      >
        <ShoppingCart />
      </Provider>
    );
    const btn = wrapper.find('button.btn-primary').first();
    btn.simulate('click');
    expect(localStorageHelpper.setLocalStorage).toHaveBeenCalled();
  });

  it('Testing testing buy click with wrong quantity', () => {
    global.document.getElementById = jest.fn().mockReturnValue({ value: NaN });
    global.window.alert = jest.fn();
    localStorageHelpper.getLocalStorage.mockReturnValue([
      products[0].id,
      products[1].id,
      products[2].id,
    ]);
    reselect.createStructuredSelector.mockReturnValue({
      products: products,
      shoppingCart: products.slice(0, 3),
    })
    wrapper = mount(
      <Provider
        store={store}
      >
        <ShoppingCart />
      </Provider>
    );
    const btn = wrapper.find('button.btn-primary').first();
    btn.simulate('click');
    expect(global.window.alert).toHaveBeenCalled();
    expect(global.window.alert).lastCalledWith('No disponemos de la cantidad seleccionada.');
  });

  it('Testing testing delete click', () => {
    global.window.alert = jest.fn();
    localStorageHelpper.getLocalStorage.mockReturnValue([
      products[0].id,
      products[1].id,
      products[2].id,
    ]);
    reselect.createStructuredSelector.mockReturnValue({
      products: products,
      shoppingCart: products.slice(0, 3),
    })
    wrapper = mount(
      <Provider
        store={store}
      >
        <ShoppingCart />
      </Provider>
    );
    const btn = wrapper.find('button.btn-secondary').first();
    btn.simulate('click');
    expect(global.window.alert).toHaveBeenCalled();
    expect(global.window.alert).lastCalledWith('Producto eliminado');
    expect(localStorageHelpper.setLocalStorage).toHaveBeenCalled();
  });
});
