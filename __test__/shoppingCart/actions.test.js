import shoppingCart from '../../src/shoppingCart';
const shoppingCartActions = shoppingCart.actions;
const shoppingCartActionTypes = shoppingCart.actionTypes;

describe('Testing the shoppingCart actions', () => {
  it('Testing setShoppingCart', () => {
    const payloadObject = [{ id: 1 }, { id: 2 }];
    const products = shoppingCartActions.setShoppingCart(payloadObject);
    expect(products).toEqual({
      type: shoppingCartActionTypes.SET_SHOPPING_CART,
      payload: payloadObject,
    });
  });
});
