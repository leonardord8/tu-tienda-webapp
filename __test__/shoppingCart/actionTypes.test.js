import shoppingCart from '../../src/shoppingCart';
const shoppingCartActionTypes = shoppingCart.actionTypes;

describe('Tesing action types', () => {
  it('testing SET_SHOPPING_CART const', () => {
    expect(shoppingCartActionTypes.SET_SHOPPING_CART).toMatch('SET_SHOPPING_CART');
  });

  it('testing action types length', () => {
    expect(Object.keys(shoppingCartActionTypes).length).toBe(1);
  });
});
