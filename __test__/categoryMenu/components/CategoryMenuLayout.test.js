import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import CategoryMenuLayout from '../../../src/categoryMenu/components/CategoryMenuLayout.jsx';

Enzyme.configure({ adapter: new Adapter() });

describe('Testing HeaderLayout.jsx', () => {
  it('Testing layout', () => {
    const wrapper = mount(
      <CategoryMenuLayout>
        <p>Test</p>
      </CategoryMenuLayout>
    );
    const ul = wrapper.find('.nav.site-nav');
    expect(ul).toHaveLength(1);
    const li = wrapper.find('.flyout');
    expect(li).toHaveLength(1);
    const a = wrapper.find('a');
    expect(a.text()).toMatch('Categorias')
    const ul2 = wrapper.find('.flyout-content.nav.stacked');
    expect(ul2).toHaveLength(1);
    const child = wrapper.find('p');
    expect(child.text()).toMatch('Test');
  });
});
