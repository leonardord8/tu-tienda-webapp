import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { BrowserRouter as Router } from 'react-router-dom';
import Enzyme, { mount } from 'enzyme';
import CategoryWrapper from '../../../src/categoryMenu/components/CategoryWrapper.jsx';

Enzyme.configure({ adapter: new Adapter() });

describe('Testing HeaderLayout.jsx', () => {
  it('Testing layout', () => {
    const wrapper = mount(
      <Router>
        <CategoryWrapper
          name='Test'
          sublevels={[
            {
              id: 1,
              name: 'TestSub',
            }
          ]}
        />
      </Router>
    );
    const li = wrapper.find('.flyout-alt.category');
    expect(li).toHaveLength(1);
    const a = wrapper.find('a').first();
    expect(a.text()).toMatch('Test');
    const a2 = wrapper.find('a').at(1);
    expect(a2.text()).toMatch('Test');
    const ul = wrapper.find('.flyout-content.nav.stacked');
    expect(ul).toHaveLength(1);
    const ul2 = wrapper.find('.flyout-content.nav.stacked');
    expect(ul2).toHaveLength(1);
  });
});
