import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { BrowserRouter as Router } from 'react-router-dom';
import Enzyme, { mount } from 'enzyme';
import SubCategoryWrapper from '../../../src/categoryMenu/components/SubCategoryWrapper.jsx';

Enzyme.configure({ adapter: new Adapter() });

describe('Testing HeaderLayout.jsx', () => {
  it('Testing layout without sublevels', () => {
    const wrapper = mount(
      <Router>
        <SubCategoryWrapper
          id={0}
          name='Test'
        />
      </Router>
    );
    const li = wrapper.find('li');
    expect(li).toHaveLength(1);
    const a = wrapper.find('a');
    expect(a.text()).toMatch('Test');
  });
  it('Testing layout with sublevels', () => {
    const wrapper = mount(
      <Router>
        <SubCategoryWrapper
          id={0}
          name='Test'
          sublevels={[
            {
              id:1,
              name:'SubTest',
            }
          ]}
        />
      </Router>
    );
    const li = wrapper.find('li');
    expect(li).toHaveLength(2);
    const a = wrapper.find('a').first();
    expect(a.text()).toMatch('Test');
    const ul = wrapper.find('.flyout-content.nav.stacked');
    expect(ul).toHaveLength(1);
    const a2 = wrapper.find('a').at(1);
    expect(a2.text()).toMatch('SubTest');
  });
});
