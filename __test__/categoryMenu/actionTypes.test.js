import categoryMenu from '../../src/categoryMenu';
const categoryMenuActionTypes = categoryMenu.actionTypes;

describe('Tesing action types', () => {
  it('testing SET_CATEGORIES const', () => {
    expect(categoryMenuActionTypes.SET_CATEGORIES).toMatch('SET_CATEGORIES');
  });

  it('testing action types length', () => {
    expect(Object.keys(categoryMenuActionTypes).length).toBe(1);
  });
});
