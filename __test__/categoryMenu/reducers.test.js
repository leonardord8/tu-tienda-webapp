import categoryMenu from '../../src/categoryMenu';
const categoryMenuReducers = categoryMenu.reducers;
const categoryMenuActionTypes = categoryMenu.actionTypes;

describe('Testing categoryMenu reducers', () => {
  it('Testing initial state', () => {
    expect(categoryMenuReducers(undefined, {})).toEqual({
      categories: [],
    });
  });

  it('Testing setCategoriesReducer', () => {
    expect(categoryMenuReducers(undefined, {
      type: categoryMenuActionTypes.SET_CATEGORIES,
      payload: [{id:1}],
    })).toEqual({
      categories: [{ id: 1 }],
    });
  });
});
