import categoryMenu from '../../src/categoryMenu';
const categoryMenuSelectors = categoryMenu.selectors;

describe('Testing categoryMenu selectors', () => {
  it('Testing getCategories', () => {
    expect(categoryMenuSelectors({ 'categoryMenu': { 'categories': [{ id: 1 }] } })).toEqual([{ id: 1 }]);
  });
});
