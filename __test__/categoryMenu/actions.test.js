import categoryMenu from '../../src/categoryMenu';
const categoryMenuActions = categoryMenu.actions;
const categoryMenuActionTypes = categoryMenu.actionTypes;

describe('Testing the categoryMenu actions', () => {
  it('Testing setCategories', () => {
    const itemQuantitySetted = categoryMenuActions.setCategories([{ id: 1 }, { id: 2 }]);
    expect(itemQuantitySetted).toEqual({
      type: categoryMenuActionTypes.SET_CATEGORIES,
      payload: [{ id: 1 }, { id: 2 }]
    });
  });
});
