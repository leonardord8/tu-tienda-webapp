import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { BrowserRouter as Router } from 'react-router-dom';
import Enzyme, { mount } from 'enzyme';
import { Provider } from 'react-redux';
import categoriesClient from '../../src/clients/categoriesClient';
import categoryMenu from '../../src/categoryMenu';
import store from '../../src/store';

Enzyme.configure({ adapter: new Adapter() });
jest.mock('../../src/clients/categoriesClient');

describe('Testing CategoryMenu.jsx', () => {
  const CategoryMenu = categoryMenu.CategoryMenu;
  let wrapper = null;
  beforeEach(() => {
    wrapper = mount(
      <Provider
        store={store}
      >
        <Router>
          <CategoryMenu />
        </Router>
      </Provider>
    );
    jest.resetAllMocks();
  });

  it('Testing layout without categories', () => {
    categoriesClient.getCategories.mockResolvedValue([]);
    const div = wrapper.find('.nav.stacked');
    expect(div).toHaveLength(1);
  });

  it('Testing Category menu with one category', () => {
    categoriesClient.getCategories.mockResolvedValue([
      {
        id: 1,
        name: 'Test',
        sublevels: [
          {
            id: 2,
            name: 'SubTest',
          }
        ]
      }
    ]);
    wrapper = mount(
      <Provider
        store={store}
      >
        <Router>
          <CategoryMenu />
        </Router>
      </Provider>
    );
    const ul = wrapper.find('ul');
    expect(ul).toHaveLength(2);
  });
});
