const path = require('path');
const pluginProposalObject = require('@babel/plugin-proposal-object-rest-spread');
const pluginProposalClassProps = require('@babel/plugin-proposal-class-properties');

module.exports = {
  entry: {
    invie: ['@babel/polyfill', path.resolve(__dirname, 'src/Index.jsx')],
  },
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'js/bundle.js',
  },
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    watchContentBase: true,
    inline: true,
    compress: true,
    port: 8000,
    historyApiFallback: true,
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react'],
            plugins: [pluginProposalObject, pluginProposalClassProps],
          },
        },
      },
      {
        test: /\.(jpg|png|gif)$/,
        use: {
          loader: 'url-loader',
          options: {
            limit: 100000,
          },
        },
      },
      {
        test: /\.json$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'json-loader',
        },
      },
    ],
  },
};
