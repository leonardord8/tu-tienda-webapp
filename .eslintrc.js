module.exports = {
  env: {
    browser: true,
    es6: true,
  },
  extends: 'airbnb',
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
    'jest'
  ],
  rules: {
    'import/extensions': [
      'error',
      'never',
      {
        jsx: 'always',
      },
    ],
    'import/no-named-as-default': 0,
    'import/no-named-as-default-member': 0,
  },
};
