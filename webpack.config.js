const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CleanWebPackPlugin = require('clean-webpack-plugin');

const config = {
  plugins: [
    new MiniCssExtractPlugin({
      filename: './css/[name].[hash].css',
      chunkFilename: '[id].[chunkhash].css',
    }),
  ],
  optimization: {
    minimizer: [
      new UglifyJsPlugin(),
      new OptimizeCSSAssetsPlugin({}),
    ],
  },
  entry: {
    invie: ['@babel/polyfill', path.resolve(__dirname, 'src/Index.jsx')],
  },
  output: {
    path: path.resolve(__dirname, 'public/dist'),
    filename: 'js/[name].[hash].js',
    publicPath: `${path.resolve(__dirname, 'public/dist')}/`,
    chunkFilename: 'js/[id].[chunkhash].js',
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env', '@babel/preset-react'],
          },
        },
      },
      {
        test: /\.(jpg|png)$/,
        use: {
          loader: 'file-loader',
          options: {
            name: '[path][name].[hash].[ext]',
          },
        },
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
        ],
      },
    ],
  },
};

module.exports = (env) => {
  if (env.NODE_ENV === 'production') {
    config.plugins.push(
      new CleanWebPackPlugin(),
    );
  }
  return config;
};
